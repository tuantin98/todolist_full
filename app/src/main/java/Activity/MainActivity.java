package Activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vuongnguyen.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;

import java.util.ArrayList;
import java.util.List;

import Adapter.ListTaskAdapter;
import Adapter.ListWorkAdapter;
import Adapter.TaskDatabase;
import ModelObject.ListTask;
import ModelObject.ListWork;
import ModelObject.TaskItem;
import ShowDialog.DataWorkDialog;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    //widgets
    public DrawerLayout mDrawerMenu;
    public Toolbar mToolBar;
    public NavigationView mNavigation;
    public RecyclerView mRecyclerListWork, mRecyclerListTask;
    public EditText mEdtData;
    public ListWorkAdapter mWorkAdapter;
    public ListTaskAdapter mTaskAdapter;
    public FloatingActionButton btnNewTask;
    private Context mContext;

    // vars
    List<ListWork> mListWork;
    List<ListTask> mListTask;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mContext = this;
        Mapping();
        init();
        SolveToolbar();
        SolveDrawerLayout();
        SolveNavigation();

        btnNewTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,RepairDataActivity.class);
                startActivity(intent);
            }
        });


    }

    /* Ham anh xa */
    public void Mapping() {
        mDrawerMenu = findViewById(R.id.draw_layout);
        mToolBar = findViewById(R.id.toolbar);
        mNavigation = findViewById(R.id.navigation_menu);
        mRecyclerListWork = findViewById(R.id.list_work);
        mRecyclerListTask = findViewById(R.id.list_task);
        mEdtData = findViewById(R.id.edt_data);
        btnNewTask = findViewById(R.id.fab_action) ;
    }

    public void init() {
        ArrayList<TaskItem> taskItems = TaskDatabase.getInstance(mContext).getTaskList();

        mListWork = new ArrayList<>();
        mListTask = new ArrayList<>();

        for (TaskItem taskItem: taskItems) {
            mListTask.add(new ListTask(taskItem.getTitle(), taskItem.getReminderType() + " - " + taskItem.getPriority(), String.valueOf(taskItem.getId()),taskItem.getCompleted()) );
        }

        mWorkAdapter = new ListWorkAdapter(this, mListWork);
        mTaskAdapter  = new ListTaskAdapter(this,mListTask);

        @SuppressLint("WrongConstant") LinearLayoutManager layoutWork = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRecyclerListWork.setLayoutManager(layoutWork);
        mRecyclerListWork.setAdapter(mWorkAdapter);
        @SuppressLint("WrongConstant") LinearLayoutManager layoutTask = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        mRecyclerListTask.setLayoutManager(layoutTask);
        mRecyclerListTask.setAdapter(mTaskAdapter);

    }

    public void SolveToolbar() {
        setSupportActionBar(mToolBar);
    }

    public void SolveDrawerLayout() {
        ActionBarDrawerToggle mAction = new ActionBarDrawerToggle(this, mDrawerMenu, mToolBar, R.string.drawer_open, R.string.drawer_close);
        mDrawerMenu.addDrawerListener(mAction);
        mAction.syncState();
    }

    public void SolveNavigation() {
        mNavigation.setNavigationItemSelectedListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.new_list:
                AddList();
                break;
            case R.id.settings:
                Toast.makeText(getApplicationContext(), "Setting", Toast.LENGTH_SHORT).show();
                break;
        }
        return false;
    }

    private void AddList() {
        mDrawerMenu.closeDrawer(GravityCompat.START);
        //DataWorkDialogFragment dataFragment = new DataWorkDialogFragment();
        DataWorkDialog dataDialog = new DataWorkDialog();
        dataDialog.show(getSupportFragmentManager(),"fragment");

    }

    public void getData(String title, int red , int green , int blue, int color ) {
        if(!title.isEmpty()){
            Toast.makeText(getApplicationContext(),""+red+" "+green+" "+blue + " " +title,Toast.LENGTH_SHORT).show();
            mListWork.add(new ListWork(R.drawable.ic_list_black_24dp,color,red,green,blue,title));
            mToolBar.setTitle(title);
            mToolBar.setBackgroundColor(Color.rgb(red,green,blue));
            mWorkAdapter.notifyDataSetChanged();
        }
    }
}