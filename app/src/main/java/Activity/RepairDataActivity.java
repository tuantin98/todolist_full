package Activity;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.fragment.app.DialogFragment;
import com.example.vuongnguyen.R;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import Adapter.TaskDatabase;
import ModelObject.TaskItem;
import Notification.AlarmReceiver;
import Notification.NotificationReceiver;
import ShowDialog.PriorityDialog;
import ShowDialog.ReminderTypeDialog;

public class RepairDataActivity extends AppCompatActivity
    implements ReminderTypeDialog.IReminderTypeDialogListener, PriorityDialog.IPriorityDialogListener {
    private String TAG ="RepairDataActivity";

    //widgets
    CoordinatorLayout mCoordina ;
    Toolbar mToolbar ;
    TextView mTextName;
    EditText mEdtNote;
    EditText mEdtData ;
    TextView mClickTIme;
    CheckBox mCbCompleted;
    TextView mTxtReminderType,mTxtPriority;
    FloatingActionButton btnSubmit;
    DialogFragment dialogFragmentReminderType;
    DialogFragment dialogFragmentPriority;
    Context mContext;

    LinearLayout layoutNewNote;
    //vars
    private TaskItem taskItem;
    private String mTitle;
    private String mNote;
    private Long mTime;
    private int mYear,mMonth,mDate,mHour,mMinute;
    private String mReminderType;
    private String mPriority;
    private final int defaultCompleted = 0;
    private int mID;
    private String mListName ="Default";
    private Boolean mFlag; // flag description for update or create
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repairdata_activity);
        mapping();
        AddData();
        mContext = this;
        final Intent intent = getIntent();
        mFlag = intent.hasExtra("ID");

        if(mFlag) {
            // view data on screen
            taskItem = TaskDatabase.getInstance(mContext).getTaskByID(   Integer.parseInt(  intent.getStringExtra("ID")  )   );
            mEdtData.setText(taskItem.getTitle());
            mEdtNote.setText(taskItem.getNote());
            mClickTIme.setText( getDateTimeString(taskItem.getDueDate()) );
            mTxtReminderType.setText(taskItem.getReminderType());
            mTxtPriority.setText(taskItem.getPriority());
            if ( taskItem.getCompleted() == 0 ) mCbCompleted.setChecked(false);
                else mCbCompleted.setChecked(true);
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // get information for information
                mTitle = mEdtData.getText().toString();
                mNote = mEdtNote.getText().toString();
                mTime = getTimeMiliseconds(mYear, mMonth, mDate, mHour, mMinute);
//                Log.d(TAG, "date & time: "+getDateTimeString(mTime));
                mReminderType = mTxtReminderType.getText().toString();
                mPriority = mTxtPriority.getText().toString();

                if(mFlag) {
                    // update task
                    mID = Integer.parseInt(intent.getStringExtra("ID"));
                    switch (mReminderType){
                        case "None":
                            break;
                        case "Notification":
                            AlarmManager alarmManager1 = (AlarmManager)getSystemService(ALARM_SERVICE);
                            Intent intent = new Intent(RepairDataActivity.this, NotificationReceiver.class);
                            PendingIntent pendingIntent = PendingIntent.getBroadcast(RepairDataActivity.this, mID, intent,PendingIntent.FLAG_CANCEL_CURRENT );
                            if (alarmManager1 != null) {
                                alarmManager1.cancel(pendingIntent);
                            }
                            Log.d(TAG,"notify delete");
                            break;
                        case "Alarm":
                            break;
                        default:
                            break;
                    }

                    TaskDatabase.getInstance(mContext).update(new TaskItem(mID,mTitle,mNote,mTime,mReminderType,mPriority,defaultCompleted,mListName));
                } else {
                    // create new task insert to database
                    mID = (int)TaskDatabase.getInstance(mContext).insert(new TaskItem(mTitle,mNote,mTime,mReminderType,mPriority,defaultCompleted,mListName));
                }

                showAllColumns();

                // set notification and alarm
                switch (mReminderType){
                    case "None":
                        break;
                    case "Notification":
                        setNotification(mID,mTitle,mNote,mTime);
                        break;
                    case "Alarm":
                        setAlarm(mID,mTime);
                        break;
                    default:
                        break;
                }
                Intent intent = new Intent(RepairDataActivity.this,MainActivity.class);

                startActivity(intent);

            }
        });

        mClickTIme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PickDate();
            }
        });

        mTxtPriority.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showPriorityDialog();
            }
        });

        mTxtReminderType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showReminderTypeDialog();
            }
        });

        layoutNewNote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mEdtNote.setFocusableInTouchMode(true);
                mEdtNote.setFocusable(true);
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                if (inputMethodManager != null) {
                    inputMethodManager.showSoftInput(mEdtNote, 0);
                }
                mEdtNote.requestFocus();
            }
        });

    }

    void mapping(){
        mCoordina = findViewById(R.id.coordinatorlayout);
        mToolbar = findViewById(R.id.toolbar_title);
        mTextName = findViewById(R.id.txt_name);
        mEdtData = findViewById(R.id.edt_data);
        btnSubmit = findViewById(R.id.btn_submit);
        mEdtNote = findViewById(R.id.edt_note);
        mClickTIme =findViewById(R.id.txt_click_time);
        mTxtReminderType = findViewById(R.id.txt_reminder);
        mTxtPriority = findViewById(R.id.txt_priority);

        layoutNewNote = findViewById(R.id.layout_add_note);
        mCbCompleted = findViewById(R.id.checkbox_status);
    }

    private void setNotification(int id,String title, String note, long time){
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        Intent intent = new Intent(RepairDataActivity.this, NotificationReceiver.class);
        intent.putExtra("ID", String.valueOf(id)  );
        intent.putExtra("NAME", title);
        intent.putExtra("NOTES", note);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(RepairDataActivity.this,id,intent,0);
        if (Build.VERSION.SDK_INT >=  Build.VERSION_CODES.KITKAT)  alarmManager.setExact(AlarmManager.RTC_WAKEUP,time, pendingIntent);
        else alarmManager.set(AlarmManager.RTC_WAKEUP,time, pendingIntent);
        Log.d(TAG,"set finish notify");
    }
    private void setAlarm(int id,long time){
        AlarmManager alarmManager = (AlarmManager)getSystemService(ALARM_SERVICE);
        Intent intent1 = new Intent(RepairDataActivity.this, AlarmReceiver.class);
        PendingIntent pendingIntent1 = PendingIntent.getBroadcast(RepairDataActivity.this,id,intent1,0);
        if (Build.VERSION.SDK_INT >=  Build.VERSION_CODES.KITKAT) {
            assert alarmManager != null;
            alarmManager.setExact(AlarmManager.RTC_WAKEUP,time, pendingIntent1);
        }
        else if (alarmManager != null) {
            alarmManager.set(AlarmManager.RTC_WAKEUP,time, pendingIntent1);
            Log.e(TAG, "Alarm manager not working");
        }
    }
    void PickDate(){
        final Calendar calendar = Calendar.getInstance();
        int year1 = calendar.get(Calendar.YEAR);
        int month1 = calendar.get(Calendar.MONTH);
        int date1 = calendar.get(Calendar.DATE);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int date) {
                mYear = year;
                mMonth = month;
                mDate = date;
                PickTime();
                calendar.set(year,month,date);
                @SuppressLint("SimpleDateFormat") SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
                mClickTIme.setText(simpleDateFormat.format(calendar.getTime())+" || ");

            }
        },year1,month1,date1);
        datePickerDialog.show();
    }

    void PickTime(){
        final Calendar calendar = Calendar.getInstance();
        int hour1  = calendar.get(Calendar.HOUR_OF_DAY);
        int minute1 = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hour, int minute) {
                mHour = hour;
                mMinute = minute;
                calendar.set(0,0,0,hour,minute,0);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
                mClickTIme.append(simpleDateFormat.format(calendar.getTime()));
            }
        },hour1, minute1,true);
        timePickerDialog.show();
    }

    private long getTimeMiliseconds(int year, int month, int date, int hour, int minute){
        Calendar calendar1 = Calendar.getInstance();
        calendar1.set(Calendar.YEAR, year);
        calendar1.set(Calendar.MONTH,month);
        calendar1.set(Calendar.DAY_OF_MONTH,date);
        calendar1.set(Calendar.HOUR_OF_DAY,hour);
        calendar1.set(Calendar.MINUTE,minute);
        calendar1.set(Calendar.SECOND,0);
        return calendar1.getTimeInMillis();
    }

    private String getDateTimeString(long time) {
        @SuppressLint("SimpleDateFormat") SimpleDateFormat simple = new SimpleDateFormat("EEE, d MMM yyyy HH:mm");
        Date result = new Date(time);
        return simple.format(result);
    }

    private void showReminderType() {
        mTxtReminderType.setText(mReminderType);
    }

    private void showPriority() {
        mTxtPriority.setText(mPriority);
    }

    void AddData(){
        setSupportActionBar(mToolbar);
        ActionBar actionBar ;
        actionBar = getSupportActionBar();
        assert actionBar != null;
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void sendPriority(String input) {
        mPriority = input;
        showPriority();
    }

    @Override
    public void sendInput(String input) {
        Log.d(TAG, "sendInput: got the input: " + input);
        mReminderType = input;
        showReminderType();
    }

    @Override
    public void onDialogNegativeClick(DialogFragment dialog) {

    }
    private void showReminderTypeDialog() {
        dialogFragmentReminderType = new ReminderTypeDialog();
        dialogFragmentReminderType.show(getSupportFragmentManager(), "ReminderTypeDialogFragment");
    }

    private void showPriorityDialog() {
        dialogFragmentPriority = new PriorityDialog();
        dialogFragmentPriority.show(getSupportFragmentManager(), "PriorityDialogFragment");
    }
    private void showAllColumns (){
        ArrayList<TaskItem> taskItems;
        taskItems = TaskDatabase.getInstance(mContext).getTaskList();
        String strLog = "";
        for (TaskItem taskItem: taskItems
             ) {
            strLog += taskItem.toString() + "\n";
        }
        Log.d(TAG, strLog);
    }

}
