package ModelObject;

public class ListWork {
    int image ;
    int color ;
    int red ;
    int blue ;
    int green ;
    String title ;

    public ListWork(int image, int color, int red, int blue, int green, String title) {
        this.image = image;
        this.color = color;
        this.red = red;
        this.blue = blue;
        this.green = green;
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public int getRed() {
        return red;
    }

    public void setRed(int red) {
        this.red = red;
    }

    public int getBlue() {
        return blue;
    }

    public void setBlue(int blue) {
        this.blue = blue;
    }

    public int getGreen() {
        return green;
    }

    public void setGreen(int green) {
        this.green = green;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
