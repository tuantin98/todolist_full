package ModelObject;

public class TaskItem {
    private int id;
    private String title;
    private String note;
    private long dueDate;
    private String reminderType;
    private String priority;
    private int completed;
    private String listName;

    public TaskItem(int id, String title, String note, long dueDate, String reminderType, String priority, int completed, String listName) {
        this.id = id;
        this.title = title;
        this.note = note;
        this.dueDate = dueDate;
        this.reminderType = reminderType;
        this.priority = priority;
        this.completed = completed; //  0:  not completed,  1: completed
        this.listName = listName;
    }

    public TaskItem(String title, String note, long dueDate, String reminderType, String priority, int completed, String listName) {
        this.title = title;
        this.note = note;
        this.dueDate = dueDate;
        this.reminderType = reminderType;
        this.priority = priority;
        this.completed = completed;
        this.listName =  listName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public long getDueDate() {
        return dueDate;
    }

    public void setDueDate(long dueDate) {
        this.dueDate = dueDate;
    }

    public String getReminderType() {
        return reminderType;
    }

    public void setReminderType(String reminderType) {
        this.reminderType = reminderType;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public String getListName() {
        return listName;
    }

    public void setListName(String listName) {
        this.listName = listName;
    }

    @Override
    public String toString() {
        return "TaskItem{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", note='" + note + '\'' +
                ", dueDate=" + dueDate +
                ", reminderType='" + reminderType + '\'' +
                ", priority='" + priority + '\'' +
                ", completed=" + completed +
                ", listName='" + listName + '\'' +
                '}';
    }
}
