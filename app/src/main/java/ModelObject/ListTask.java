package ModelObject;

public class ListTask {
    private String title;
    private String info;
    private String id;
    private int completed;

    public ListTask(String title, String info, String id, int completed) {
        this.info = info;
        this.title = title;
        this.id = id;
        this.completed = completed;
    }

    public int getCompleted() {
        return completed;
    }

    public void setCompleted(int completed) {
        this.completed = completed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
