package Adapter;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;

import ModelObject.TaskItem;

public class TaskDatabase {
    private TaskSQliteOpenHelper taskSQliteOpenHelper;
    private TaskDatabase(Context context){
        taskSQliteOpenHelper = new TaskSQliteOpenHelper(context);
    }
    private static TaskDatabase instance;
    public static TaskDatabase getInstance(Context context){
        if (instance == null )
            synchronized (TaskDatabase.class){
                if (instance == null){
                    instance = new TaskDatabase(context);
                }
            }
        return instance;
    }
    public long insert(TaskItem taskItem){
        SQLiteDatabase sqLiteDatabase = instance.taskSQliteOpenHelper.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(TaskSQliteOpenHelper.TITLE, taskItem.getTitle());
        contentValues.put(TaskSQliteOpenHelper.NOTE, taskItem.getNote());
        contentValues.put(TaskSQliteOpenHelper.DUE_DATE, taskItem.getDueDate());
        contentValues.put(TaskSQliteOpenHelper.REMINDER_TYPE, taskItem.getReminderType());
        contentValues.put(TaskSQliteOpenHelper.PRIORITY, taskItem.getPriority());
        contentValues.put(TaskSQliteOpenHelper.COMPLETED, taskItem.getCompleted());
        contentValues.put(TaskSQliteOpenHelper.LIST_NAME, taskItem.getListName());
        long id = sqLiteDatabase.insert(TaskSQliteOpenHelper.TASK_TABLE,null, contentValues);
        sqLiteDatabase.close();
        return id;
    }
    public ArrayList< TaskItem > getTaskList(){
        ArrayList <TaskItem> arrTask = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = instance.taskSQliteOpenHelper.getReadableDatabase();
        String[] columns = taskSQliteOpenHelper.getAllColumns();
        Cursor cursor = sqLiteDatabase.query(taskSQliteOpenHelper.TASK_TABLE, columns,
                null,null,null,null,null);
        while (cursor.moveToNext()){
            int id  = cursor.getInt(0);
            String title = cursor.getString(1);
            String note = cursor.getString(2);
            long dueDate = cursor.getLong(3);
            String reminderType = cursor.getString(4);
            String priority = cursor.getString(5);
            int completed = cursor.getInt(6);
            String listName = cursor.getString(7);
            arrTask.add(new TaskItem(id, title, note, dueDate, reminderType, priority, completed,listName));
        }
        cursor.close();
        return arrTask;
    }

    public TaskItem getTaskByID(int id) {
        SQLiteDatabase sqLiteDatabase = instance.taskSQliteOpenHelper.getReadableDatabase();
        String[] columns = taskSQliteOpenHelper.getAllColumns();
        Cursor cursor = sqLiteDatabase.query(taskSQliteOpenHelper.TASK_TABLE, columns,
                TaskSQliteOpenHelper.ID + " = " + id,null,null,null,null);
        cursor.moveToNext();
        String title = cursor.getString(1);
        String note = cursor.getString(2);
        long dueDate = cursor.getLong(3);
        String reminderType = cursor.getString(4);
        String priority = cursor.getString(5);
        int completed = cursor.getInt(6);
        String listName = cursor.getString(7);
        return  new TaskItem(id,title,note,dueDate,reminderType,priority,completed,listName);
    }

    public void update( TaskItem taskItem){
        SQLiteDatabase sqLiteDatabase = instance.taskSQliteOpenHelper.getWritableDatabase();
        ContentValues contentValues = new ContentValues();

        contentValues.put(TaskSQliteOpenHelper.TITLE, taskItem.getTitle());
        contentValues.put(TaskSQliteOpenHelper.NOTE, taskItem.getNote());
        contentValues.put(TaskSQliteOpenHelper.DUE_DATE, taskItem.getDueDate());
        contentValues.put(TaskSQliteOpenHelper.REMINDER_TYPE, taskItem.getReminderType());
        contentValues.put(TaskSQliteOpenHelper.PRIORITY, taskItem.getPriority());
        contentValues.put(TaskSQliteOpenHelper.COMPLETED, taskItem.getCompleted());
        contentValues.put(TaskSQliteOpenHelper.LIST_NAME, taskItem.getListName());


        String whereClause = TaskSQliteOpenHelper.ID + " = ?"; //+ MySQLiteOpenHelper.TITLE + "  = ?"
        String[] whereArgs = new String[]{String.valueOf(taskItem.getId())};
        sqLiteDatabase.update(TaskSQliteOpenHelper.TASK_TABLE,contentValues,whereClause,whereArgs);
    }

    public void delete(int id){
        SQLiteDatabase sqLiteDatabase = instance.taskSQliteOpenHelper.getWritableDatabase();
        String whereClause = taskSQliteOpenHelper.ID + " = ?"; //+ MySQLiteOpenHelper.TITLE + "  = ?"
        String[] whereArgs = new String[]{String.valueOf(id)};
        sqLiteDatabase.delete(taskSQliteOpenHelper.TASK_TABLE,whereClause,whereArgs);
    }

    public ArrayList <TaskItem> getTaskByListName(String _listName){
        ArrayList <TaskItem> arrTask = new ArrayList<>();
        SQLiteDatabase sqLiteDatabase = instance.taskSQliteOpenHelper.getReadableDatabase();
        String[] columns = taskSQliteOpenHelper.getAllColumns();
        Cursor cursor = sqLiteDatabase.query(taskSQliteOpenHelper.TASK_TABLE, columns,
                 null,null,null,null,null);

        while (cursor.moveToNext()){
            int id  = cursor.getInt(0);
            String title = cursor.getString(1);
            String note = cursor.getString(2);
            long dueDate = cursor.getLong(3);
            String reminderType = cursor.getString(4);
            String priority = cursor.getString(5);
            int completed = cursor.getInt(6);
            String listName = cursor.getString(7);
            if ( listName.equals(_listName))
            arrTask.add(new TaskItem(id, title, note, dueDate, reminderType, priority, completed,listName));
        }
        cursor.close();
        return arrTask;
    }

}
