package Adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.vuongnguyen.R;

import java.util.HashMap;
import java.util.List;

import ModelObject.ListParent;

public class ListDataAdapter extends BaseExpandableListAdapter {

    Context mContext ;
    List<ListParent> mListHead ;
    HashMap<ListParent,List<ListParent>>mListChild ;

    public ListDataAdapter(Context mContext, List<ListParent> mListHead, HashMap<ListParent, List<ListParent>> mListChild) {
        this.mContext = mContext;
        this.mListHead = mListHead;
        this.mListChild = mListChild;
    }
    @Override
    public int getGroupCount() {

        return mListHead == null ? 0 : mListHead.size() ;
    }

    @Override
    public int getChildrenCount(int i) {
        return mListChild.get(mListHead.get(i)).size();
    }

    @Override
    public Object getGroup(int i) {
        return mListHead.get(i);
    }

    @Override
    public Object getChild(int i, int i1) {
        return mListChild.get(mListHead.get(i)).get(i1);
    }

    @Override
    public long getGroupId(int i) {
        return i;
    }

    @Override
    public long getChildId(int i, int i1) {
        return i1;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int i, boolean b, View view, ViewGroup viewGroup) {
        if(i == 0){
            ListParent temp = (ListParent) getGroup(i);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.note_data,null);
            ImageView mImageIcon = view.findViewById(R.id.image_icon2);
            TextView mTextTitle =  view.findViewById(R.id.txt_notes);
            EditText mEdtData   = view.findViewById(R.id.edt_data);
            mImageIcon.setBackgroundResource(temp.getImage());
            mTextTitle.setText(temp.getTitle());
        }
        else if(i < 4 && i >= 1) {
            ListParent temp = (ListParent) getGroup(i);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.list_parent_data,null);
            ImageView mImageIcon = view.findViewById(R.id.image_icon);
            TextView mTextTitle =  view.findViewById(R.id.txtview_title);
            TextView mTextName = view.findViewById(R.id.txt_name);
            mImageIcon.setBackgroundResource(temp.getImage());
            mTextTitle.setText(temp.getTitle());
            mTextName.setText(temp.getName()); ;
        }
        else {
            ListParent temp = (ListParent) getGroup(i);
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.complete_checbox,null);
            TextView mTextTitle =  view.findViewById(R.id.txt_completed);
            CheckBox mCheckBox = view.findViewById(R.id.cb_complete);
            mTextTitle.setText(temp.getTitle());
        }
        return view;
    }

    @Override
    public View getChildView(int i, int i1, boolean b, View view, ViewGroup viewGroup) {
        ListParent temp = (ListParent) getChild(i,i1);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.list_child_data,null);
        ImageView mImageIcon = view.findViewById(R.id.image_icon1);
        TextView mTextTitle =  view.findViewById(R.id.txtview_title1);
        TextView mTextName = view.findViewById(R.id.txt_name1);
        mImageIcon.setBackgroundResource(temp.getImage());
        mTextTitle.setText(temp.getTitle());
        mTextName.setText(temp.getName()); ;
        return view;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }
}
