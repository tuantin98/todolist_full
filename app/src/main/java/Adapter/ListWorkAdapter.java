package Adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.vuongnguyen.R;

import java.util.List;

import ModelObject.ListWork;

public class ListWorkAdapter extends RecyclerView.Adapter<ListWorkAdapter.ViewHolder> {
    Context mContext;

    List<ListWork> mListWork;

    public ListWorkAdapter(Context mContext, List<ListWork> mListWork) {
        this.mContext = mContext;
        this.mListWork = mListWork;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.list_work, parent, false);
        ViewHolder mView = new ViewHolder(view);
        return mView;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        int color = Color.rgb(mListWork.get(position).getRed(), mListWork.get(position).getGreen(), mListWork.get(position).getBlue());
        Drawable mDrawable = ContextCompat.getDrawable(mContext, R.drawable.ic_list_black_24dp);
        mDrawable.setColorFilter(new PorterDuffColorFilter(color, PorterDuff.Mode.SRC_ATOP));

        holder.mImageIcon.setImageDrawable(mDrawable);
        holder.mTextWork.setText(mListWork.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return mListWork.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView mImageIcon;
        TextView mTextWork;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageIcon = itemView.findViewById(R.id.image_icon);
            mTextWork = itemView.findViewById(R.id.txt_title);
        }
    }
}