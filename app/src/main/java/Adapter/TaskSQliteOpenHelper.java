package Adapter;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TaskSQliteOpenHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "task.db";
    private static final Integer DATABASE_VERSION = 1;
    public static final String  TASK_TABLE = "task_table";
    public static final String  ID = "id";
    public static final String  TITLE = "title";
    public static final String  NOTE = "note";
    public static final String  DUE_DATE = "dueDate";
    public static final String  REMINDER_TYPE ="reminderType";
    public static final String  PRIORITY ="priority";
    public static final String  COMPLETED ="completed";
    public static final String  LIST_NAME = "listName";
    TaskSQliteOpenHelper(Context context){
        super(context,DATABASE_NAME,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String  CREATE_SQL = String.format("CREATE TABLE %s ("+
                "%s INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "%s TEXT,"+
                "%s TEXT,"+
                "%s LONG,"+
                "%s TEXT," +
                "%s TEXT, " +
                "%s INTEGER, "+
                "%s TEXT)",
                TASK_TABLE, ID, TITLE, NOTE, DUE_DATE, REMINDER_TYPE, PRIORITY, COMPLETED,LIST_NAME);
        sqLiteDatabase.execSQL(CREATE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    public String[] getAllColumns(){
        return new String[] { ID,TITLE,NOTE,DUE_DATE,REMINDER_TYPE,PRIORITY,COMPLETED,LIST_NAME } ;

    }
}
