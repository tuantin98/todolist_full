package Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.vuongnguyen.R;
import java.util.List;

import Activity.RepairDataActivity;
import ModelObject.ListTask;

public class ListTaskAdapter extends RecyclerView.Adapter<ListTaskAdapter.ViewHolder> {

    private Context mContext ;
    private List<ListTask> mList ;
    private boolean flag = false ;
    public ListTaskAdapter(Context mContext, List<ListTask> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.complete_checbox,parent,false);

        return new ViewHolder(view) ;
    }


    // Code trigger event completed, delete, update task in here
    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.mTxtTitle.setText(mList.get(position).getTitle());
        holder.mTxtInfo.setText(mList.get(position).getInfo());
        holder.mTxtID.setText(mList.get(position).getId());
        //set checked
//        flag = mList.get(position).getCompleted() != 0;
        if(!flag){
            holder.mCheckBox.setChecked(false);
        } else {
            holder.mCheckBox.setChecked(true);
        }


        holder.mLayoutInfoTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //go on
                String id = mList.get(position).getId();
                Intent intent = new Intent(mContext, RepairDataActivity.class);
                intent.putExtra("ID",id);
                mContext.startActivity(intent);

                Toast.makeText(mContext, "id: "+id, Toast.LENGTH_SHORT).show();
            }
        });

        holder.mLayoutDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = mList.get(position).getId();
//                int id = Integer.parseInt(idStr);

                Toast.makeText(mContext, "id in delete: " + id, Toast.LENGTH_SHORT).show();
            }
        });



        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    holder.mTxtTitle.setVisibility(View.GONE);
                    flag = false ;
                }else {
                    holder.mTxtTitle.setVisibility(View.VISIBLE);
                    holder.mTxtTitle.setText("OKE");
                    flag = true ;
                }

            }
        });
    }
    @Override
    public int getItemCount() {
        return mList.size();
    }

    public  class  ViewHolder extends RecyclerView.ViewHolder{
        CheckBox mCheckBox ;
        TextView mTxtTitle ;
        TextView mTxtInfo;
        LinearLayout mLayoutInfoTask;
        TextView mTxtID;
        LinearLayout mLayoutDelete;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mCheckBox = itemView.findViewById(R.id.cb_complete);
            mTxtTitle = itemView.findViewById(R.id.txt_completed);
            mTxtInfo = itemView.findViewById(R.id.text_info);
            mLayoutInfoTask = itemView.findViewById(R.id.layout_info_task);
            mTxtID = itemView.findViewById(R.id.txt_id);
            mLayoutDelete = itemView.findViewById(R.id.layout_delete);
        }
    }
}
