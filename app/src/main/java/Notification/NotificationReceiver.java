package Notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class NotificationReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = Integer.parseInt(intent.getStringExtra("ID"));
        String title = intent.getStringExtra("NAME");
        String notes = intent.getStringExtra("NOTES");

        ShowNotification showNotification = new ShowNotification(id,title,notes,context);
        showNotification.CreateNotification();
    }
}
