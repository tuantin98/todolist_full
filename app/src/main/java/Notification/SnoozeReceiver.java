package Notification;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationManagerCompat;

public class SnoozeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = Integer.parseInt( intent.getStringExtra("ID"));
        String name = intent.getStringExtra("NAME");
        String notes = intent.getStringExtra("NOTES");
        NotificationManagerCompat notificationManagerCompat =  NotificationManagerCompat.from(context.getApplicationContext());
        notificationManagerCompat.cancel(id);

        Intent intent1  = new Intent(context, NotificationReceiver.class);
        intent1.putExtra("ID",String.valueOf(id));
        intent1.putExtra("NAME",name);
        intent1.putExtra("NOTES",notes);
        long time = System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context,id,intent1,0);
        AlarmManager alarmManager = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        alarmManager.setExact(AlarmManager.RTC_WAKEUP,time+60000,pendingIntent);
    }
}
