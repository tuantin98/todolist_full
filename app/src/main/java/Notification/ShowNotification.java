package Notification;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.vuongnguyen.R;

import Activity.RepairDataActivity;

public class ShowNotification {

    private String CHANNEL_ID = "notification";
    private int notifyID;
    private String title;
    private String notes;
    private Context context;

    public ShowNotification(int notifyID, String title, String notes, Context context) {
        this.notifyID = notifyID;
        this.title = title;
        this.notes = notes;
        this.context = context;
    }
    public void CreateNotification(){
        // show activity
        Intent intent = new Intent(context, RepairDataActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        int reqCode = (int)System.currentTimeMillis();
        PendingIntent pendingIntent = PendingIntent.getActivity(context, reqCode, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        // Create Button Completed in notify\

        Intent completedIntent = new Intent(context,CompletedReceiver.class);
        completedIntent.putExtra("ID",String.valueOf(this.notifyID));
        PendingIntent actionCompleted =PendingIntent.getBroadcast(context,reqCode+1, completedIntent, PendingIntent.FLAG_CANCEL_CURRENT);

        // Create Button Snooze in notify
        Intent snoozeIntent = new Intent(context,SnoozeReceiver.class);
        snoozeIntent.putExtra("ID",String.valueOf(this.notifyID));
        snoozeIntent.putExtra("NAME",title);
        snoozeIntent.putExtra("NOTES",notes);

        PendingIntent actionSnooze = PendingIntent.getBroadcast(context,reqCode+2, snoozeIntent,PendingIntent.FLAG_CANCEL_CURRENT);

        NotificationCompat.Builder mBuilder =new NotificationCompat.Builder(context, CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_playlist_add_check_black_24dp)
                .setContentTitle(title)
                .setContentText(notes)
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setColor(Color.BLUE)
                .setContentIntent(pendingIntent)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setDefaults( Notification.DEFAULT_SOUND)
                .setVibrate(new long[]{1000,1000,1000,1000,1000,1000,1000,1000,1000} )
                .addAction(R.mipmap.ic_launcher,"COMPLETED",actionCompleted)
                .addAction(R.mipmap.ic_launcher,"SNOOZE",actionSnooze)
                .setOnlyAlertOnce(true)
                .setAutoCancel(true);

        NotificationManagerCompat notificationManagerCompat =NotificationManagerCompat.from(context);
        notificationManagerCompat.notify(notifyID,mBuilder.build());
    }
}
