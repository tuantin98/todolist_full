package Notification;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationManagerCompat;

import java.util.ArrayList;

import Adapter.TaskDatabase;
import ModelObject.TaskItem;

public class CompletedReceiver extends BroadcastReceiver {

    private final int COMPLETED_CODE = 1;

    @Override
    public void onReceive(Context context, Intent intent) {
        int id = Integer.parseInt( intent.getStringExtra("ID")  );
        TaskItem taskItem = TaskDatabase.getInstance(context).getTaskByID(id);
        taskItem.setCompleted(COMPLETED_CODE);
        TaskDatabase.getInstance(context).update(taskItem);
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context.getApplicationContext());
        notificationManagerCompat.cancel(id);
        Toast.makeText(context, "The task is completed" , Toast.LENGTH_SHORT).show();


        ArrayList<TaskItem> taskItems;
        taskItems = TaskDatabase.getInstance(context).getTaskList();
        String strLog = "";
        for (TaskItem item: taskItems
        ) {
            strLog += item.toString() + "\n";
        }
        Log.d("Database", strLog);
    }

}
