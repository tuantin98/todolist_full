package ShowDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.example.vuongnguyen.R;

import java.util.Objects;


public class ReminderTypeDialog extends DialogFragment {
    private static  final String TAG = "ReminderTypeDialog";
    public interface IReminderTypeDialogListener {
        void sendInput(String input);
        void onDialogNegativeClick(DialogFragment dialog);
    }


    public IReminderTypeDialogListener priorityDialogListener;

    //vars
    private String selection;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String[] reminderTypes = Objects.requireNonNull(getActivity()).getResources().getStringArray(R.array.reminder_type);

        builder.setTitle("Task reminder type")
                .setSingleChoiceItems(reminderTypes, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        selection = reminderTypes[which];
                        Log.i("dialog pri", "Value: " + selection);

                        priorityDialogListener.sendInput(selection);

                        getDialog().dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i("dialog pri", "Cancel");
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            priorityDialogListener = (IReminderTypeDialogListener) getActivity();
        }catch (ClassCastException e) {
            Log.e(TAG, "onAttach: Class cast exception: " + e.getMessage());
        }
    }

}
