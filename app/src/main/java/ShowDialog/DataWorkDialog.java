package ShowDialog;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.example.vuongnguyen.R;

import Activity.MainActivity;

public class DataWorkDialog extends DialogFragment {
    public TextView mTextName ;
    public EditText mEdtName ;
    public ImageView mImageColor;
    public Button mBtCancel ;
    public Button mBtSave ;
    public LinearLayout mBackColorLinearLayout;
    public Bitmap bitmap ;
    public  int red , green , blue ;

    @SuppressLint("ClickableViewAccessibility")
    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_fragment_addwork,null);
        mTextName = view.findViewById(R.id.txt_name);
        mEdtName = view.findViewById(R.id.edt_work);
        mImageColor = view.findViewById(R.id.image_color) ;
        mBackColorLinearLayout = view.findViewById(R.id.linearlayout);
        builder.setView(view).setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialogInterface, int i) {

            }
        }).setNegativeButton("SAVE", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                MainActivity ac = (MainActivity) getActivity();
                String temp  = mEdtName.getText().toString();
                int color = Color.rgb(red,green,blue);
               // Toast.makeText(getContext()," "+red,Toast.LENGTH_SHORT).show();
                ac.getData(temp,red,green,blue,color);
            }
        });

        mImageColor.setDrawingCacheEnabled(true);
        mImageColor.buildDrawingCache(true);
        mImageColor.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if(motionEvent.getAction() == motionEvent.ACTION_DOWN || motionEvent.getAction() == motionEvent.ACTION_MOVE){
                    bitmap = mImageColor.getDrawingCache();
                    int pixel = bitmap.getPixel((int)motionEvent.getX(), (int)motionEvent.getY());
                     red = Color.red(pixel);
                     green = Color.green(pixel);
                     blue  = Color.blue(pixel);
                    mBackColorLinearLayout.setBackgroundColor(Color.rgb(red,green,blue));
                }
                return false;
            }
        });



        return builder.create();
    }

    public void onResume()
    {
        super.onResume();
        Window window = getDialog().getWindow();
        window.setLayout(1000, 1250);
        window.setGravity(Gravity.CENTER);
        //TODO:
        Log.d("123","oke");
    }
}
