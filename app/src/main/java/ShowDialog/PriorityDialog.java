package ShowDialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.DialogFragment;

import com.example.vuongnguyen.R;

import java.util.Objects;

public class PriorityDialog extends DialogFragment {
    private static  final String TAG = "PriorityDialog";

    public interface IPriorityDialogListener {
        void sendPriority(String input);
        void onDialogNegativeClick(DialogFragment dialog);
    }

    public IPriorityDialogListener priorityDialogListener;

    //vars
    private String selection;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @NonNull
    @Override
    public Dialog onCreateDialog(final Bundle savedInstanceState) {

        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        final String[] priorities = Objects.requireNonNull(getActivity()).getResources().getStringArray(R.array.priority);

        builder.setTitle("Task reminder type")
                .setSingleChoiceItems(priorities, -1, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int which) {
                        selection = priorities[which];
                        Log.i("dialog pri", "Value: " + selection);

                        priorityDialogListener.sendPriority(selection);

                        getDialog().dismiss();
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Log.i("dialog pri", "Cancel");
                    }
                });

        return builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            priorityDialogListener = (PriorityDialog.IPriorityDialogListener) getActivity();
        }catch (ClassCastException e) {
            Log.e(TAG, "onAttach: Class cast exception: " + e.getMessage());
        }
    }
}
